#C Sharp
---

A bucket / sandbox for C# things I am interested in, including custom PowerShell
cmdlets and some simple neural networking implementations by Jeff Heaton.

Neural nets are something I have been fascinated about for a long time and I am very interested in potential applications in the IaaS realm.

`
λD
`
