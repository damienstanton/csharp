// Template for the complete IDisposable interface pattern.
// This should be used when writing cmdlets that requires object cleanup

using System;
using System.ComponentModel;

public class Disposed
{
	public class ResourceToBeDisposed : IDisposable
	{
		// Pointer to an external unmanaged resource
		private IntPtr handle; 
		private Component component = new Component();
		private bool disposed = false;

		public ResourceToBeDisposed(IntPtr handle)
		{
			this.handle = handle;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SupressFinalize(this);
		}

		// If disposing eq true, the method is called directly or indirectly by
		// user's code. Both managed and unmanaged resources can be disposed. If
		// disposing eq false, the method has been called by the runtime from
		// inside the finalizer and other objects should not be referenced. In that
		// case, ONLY unmanaged resources can be disposed.
		private void Dispose(bool disposing)
		{
			if(!this.disposed)
			{
				if(disposing)
				{
					component.Dispose();
				}
				CloseHandle(handle);
				handle = IntPtr.Zero;
				disposed = true;
			}
		}

		[System.Runtime.InteropServices.DllImport("Kernel32")]
		private extern static Boolean CloseHandle(IntPtr handle);
		// C# destructor syntax
		~ResourceToBeDisposed() 
		{
			// "Optimal in terms of readability and maintainability"
			Dispose(false) 
		}

		public static void Main()
		{
			//TODO create and utilize the ResourceToBeDisposed object
		}
}
