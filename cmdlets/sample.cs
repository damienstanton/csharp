/* Just a scratch C# file to get things going with a custom cmdlet.
Lots to learn yet... */

using System;
using System.Diagnostics;
using System.Management.Automation;
using Microsoft.PowerShell.Commands;

namespace SendGreeting
{
	// Class declaration
	[Cmdlet(VerbsCommunications.Send, "GreetingInvoke")]
	public class SendGreetingInvokeCommand : Cmdlet
	{
		// Parameter declaration
		[Parameter(Mandatory = true)]
		public string Name
		{
			get { return name; }
			set { name = value; }
		}
		private string name;

		// Invoke the Get-Process cmdlet
		protected override void BeginProcessing()
		{
			GetProcessCommand gp = new GetProcessCommand();
			gp.Name = new string[] { "[a-t]*" };
			foreach (Process p in gp.Invoke<Process>())
			{
				Console.WriteLine(p.ToString());
			}
		}

		// Write out the greeting
		protected override void ProcessRecord()
		{
			WriteObject("Hello" + name + "!");
		}
	}
}
